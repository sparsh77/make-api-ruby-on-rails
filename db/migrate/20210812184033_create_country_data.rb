class CreateCountryData < ActiveRecord::Migration[6.1]
  def change
    create_table :country_data, :id=> false do |t|
      t.numeric :id
      t.string :code
      t.string :country
      t.boolean :publish, default: false

      t.timestamps
    end
  end
end
