class Api::V1::CountryDataController < ApplicationController
  def index
    country_data = CountryDatum.all
    render json: country_data, status: 200
  end

  def create
    country_data = CountryDatum.new(
      id: prod_params[:id],
      code: prod_params[:code],
      country: prod_params[:country],
      publish: prod_params[:publish]
    )
    if country_data.save
      render json: country_data, status: 200
    else
      render json: {error: "Error creating review."}
    end
  end

  def show
    country_data = CountryDatum.find_by(id: params[:id])
    if country_data
      render json: country_data, status: 200
    else
      render json: {error: "Country data not found."}
    end

    private
    def prod_params
      params.require(:country_data).permit([
        :id,
        :code,
        :country,
        :publish
                                           ])
    end
  end
end
